<?php

namespace Jm\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jm\EshopBundle\Entity\PurchaseItem
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PurchaseItem
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price", type="decimal")
     */
    private $price;

    /**
     * @var integer $amount
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

	/**
	 * @ORM\ManyToOne(targetEntity="Purchase")
	 */
	private $purchase;

	/**
	 * @ORM\ManyToOne(targetEntity="Product")
	 */
	private $product;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return PurchaseItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return PurchaseItem
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

	public function setProduct(Product $product)
	{
		$this->product = $product;

		return $this;
	}

	public function getProduct()
	{
		return $this->product;
	}

	public function setPurchase(Purchase $purchase)
	{
		$this->purchase = $purchase;

		return $this;
	}

	public function getPurchase()
	{
		return $this->purchase;
	}
}
