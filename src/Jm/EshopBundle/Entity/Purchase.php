<?php

namespace Jm\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jm\EshopBundle\Entity\Purchase
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Jm\EshopBundle\Entity\PurchaseRepository")
 */
class Purchase
{
	const STATE_NOT_TAKEN = 1;

	const STATE_TAKEN_NOT_PAID = 2;

	const STATE_INVOICED = 3;

	const STATE_TAKEN_PAID = 4;

	const STATE_CANCELED = 5;

	const STATE_SHIPPED = 6;

	const PAYMENT_CASH_SHOP = 1;

	const PAYMENT_CASH_ON_DELIVERY = 2;

	const PAYMENT_BANK_ACCOUNT_SHOP = 3;

	const PAYMENT_BANK_ACCOUNT_POST = 4;

	const PAYMENT_CREDIT_CARD_SHOP = 5;

	const PAYMENT_CREDIT_CARD_POST = 6;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string $address
     *
     * @ORM\Column(name="address", type="string", length=500)
     */
    private $address;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var integer $paymentMethod
     *
     * @ORM\Column(name="paymentMethod", type="integer")
     */
    private $paymentMethod;

    /**
     * @var string $variableNumber
     *
     * @ORM\Column(name="variableNumber", type="string", length=45)
     */
    private $variableNumber;

	/**
	 * @ORM\ManyToOne(targetEntity="User")
	 */
	private $user;

	public function __construct()
	{
		$this->createdAt = new \DateTime();

		$varNum = '';

		for ($i = 0; $i < 9; $i++) {
			$varNum .= rand(10000, 99999);
		}

		$this->variableNumber = $varNum;
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Purchase
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Purchase
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Purchase
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Purchase
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set paymentMethod
     *
     * @param integer $paymentMethod
     * @return Purchase
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return integer
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set variableNumber
     *
     * @param string $variableNumber
     * @return Purchase
     */
    public function setVariableNumber($variableNumber)
    {
        $this->variableNumber = $variableNumber;

        return $this;
    }

    /**
     * Get variableNumber
     *
     * @return string
     */
    public function getVariableNumber()
    {
        return $this->variableNumber;
    }

	public function setUser(User $user)
	{
		$this->user = $user;

		return $this;
	}

	public function getUser()
	{
		return $this->user;
	}
}
