<?php

namespace Jm\EshopBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
	public function getBasketItems(array $data)
	{
		$self = $this;
		return array_map(function ($item) use ($self) {
			return array(
				'amount' => $item['amount'],
				'product' => $self->find($item['id']),
			);
		}, $data);
	}
}
