<?php

namespace Jm\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jm\EshopBundle\Entity\Product
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Jm\EshopBundle\Entity\ProductRepository")
 */
class Product
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $about
     *
     * @ORM\Column(name="about", type="text")
     */
    private $about;

    /**
     * @var float $amount
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @var boolean $visible
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;


	/**
	 * @ORM\ManyToOne(targetEntity="Category")
	 */
	private $category;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set about
     *
     * @param string $about
     * @return Product
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Product
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Product
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

	public function setCategory(Category $category)
	{
		$this->category = $category;

		return $this;
	}

	public function getCategory()
	{
		return $this->category;
	}

	public function isAmountAvailable($amount)
	{
		return $this->amount > $amount;
	}

	public function decreaseAmount($amount)
	{
		$this->amount -= $amount;

		return $this;
	}

	public function increaseAmount($amount)
	{
		$this->amount += $amount;

		return $this;
	}
}
