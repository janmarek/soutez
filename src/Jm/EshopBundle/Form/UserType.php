<?php

namespace Jm\EshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
	private $passwordRequired;

	public function __construct($passwordRequired = true)
	{
		$this->passwordRequired = $passwordRequired;
	}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('plainPassword', 'password', array(
				'property_path' => false,
				'required' => $this->passwordRequired,
			))
            ->add('admin', null, array(
				'required' => false,
			))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Jm\EshopBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'jm_eshopbundle_usertype';
    }
}
