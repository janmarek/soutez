<?php

namespace Jm\EshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('about')
            ->add('amount')
            ->add('price')
            ->add('visible', null, array(
				'required' => 'false',
			))
            ->add('category', 'entity', array(
				'class' => 'JmEshopBundle:Category',
				'property' => 'name',
			))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Jm\EshopBundle\Entity\Product'
        ));
    }

    public function getName()
    {
        return 'jm_eshopbundle_producttype';
    }
}
