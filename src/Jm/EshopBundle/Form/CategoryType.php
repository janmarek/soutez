<?php

namespace Jm\EshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('url')
            ->add('parent', 'entity', array(
				'required' => false,
				'class' => 'JmEshopBundle:Category',
				'property' => 'name',
				// todo do not choose itself or its parents
			))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Jm\EshopBundle\Entity\Category'
        ));
    }

    public function getName()
    {
        return 'jm_eshopbundle_categorytype';
    }
}
