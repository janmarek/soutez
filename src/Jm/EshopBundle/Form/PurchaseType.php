<?php

namespace Jm\EshopBundle\Form;

use Jm\EshopBundle\Entity\Purchase;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PurchaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('createdAt')
            ->add('status', 'choice', array(
				'choices' => array(
					Purchase::STATE_NOT_TAKEN => 'nepřevzatá',
					Purchase::STATE_TAKEN_NOT_PAID => 'převzatá, nezaplacená',
					Purchase::STATE_INVOICED => 'fakturovaná',
					Purchase::STATE_TAKEN_PAID => 'převzatá, zaplacená',
					Purchase::STATE_CANCELED => 'zrušená',
					Purchase::STATE_SHIPPED => 'odeslaná',
				),
			))
            ->add('paymentMethod', 'choice', array(
				'choices' => array(
					Purchase::PAYMENT_CASH_SHOP => 'hotově na výdejně',
					Purchase::PAYMENT_CASH_ON_DELIVERY => 'dobírka',
					Purchase::PAYMENT_BANK_ACCOUNT_SHOP => 'bankovní převod, výdejna',
					Purchase::PAYMENT_BANK_ACCOUNT_POST => 'bankovní převod, pošta',
					Purchase::PAYMENT_CREDIT_CARD_SHOP => 'kreditka, výdejna',
					Purchase::PAYMENT_CREDIT_CARD_POST => 'kreditka, pošta',
				)
			))
            ->add('variableNumber')
            ->add('user', 'entity', array(
				'required' => false,
				'class' => 'JmEshopBundle:User',
				'property' => 'username',
			))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Jm\EshopBundle\Entity\Purchase'
        ));
    }

    public function getName()
    {
        return 'jm_eshopbundle_purchasetype';
    }
}
