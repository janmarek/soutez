<?php

namespace Jm\EshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Jm\EshopBundle\Entity\Category;
use Jm\EshopBundle\Form\CategoryType;

/**
 * @Route("/admin/category")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/", name="admin_category")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JmEshopBundle:Category')->findAll();

        return array(
            'entities' => $entities,
        );
    }

	/**
	 * @Route("/new", name="admin_category_new")
	 * @Template()
	 */
	public function newAction(Request $request)
	{
		$entity = new Category();
		$form = $this->createForm(new CategoryType(), $entity);

		if ($request->getMethod() === 'POST') {
			$form->bind($request);

			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($entity);
				$em->flush();

				return $this->redirect($this->generateUrl('admin_category_edit', array('id' => $entity->getId())));
			}
		}

		return array(
			'entity' => $entity,
			'form' => $form->createView(),
		);
	}

	/**
	 * @Route("/{id}/edit", name="admin_category_edit")
	 * @Template()
	 */
	public function editAction(Request $request, Category $entity)
	{
		$editForm = $this->createForm(new CategoryType(), $entity);
		$deleteForm = $this->createDeleteForm($entity->getId());

		if ($request->getMethod() === 'POST') {
			$editForm->bind($request);

			if ($editForm->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($entity);
				$em->flush();

				return $this->redirect($this->generateUrl('admin_category_edit', array('id' => $entity->getId())));
			}
		}

		return array(
			'entity' => $entity,
			'edit_form' => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		);
	}

	/**
	 * @Route("/{id}/delete", name="admin_category_delete")
	 * @Method("POST")
	 */
	public function deleteAction(Request $request, Category $entity)
	{
		$em = $this->getDoctrine()->getManager();
		$em->remove($entity);
		$em->flush();

		return $this->redirect($this->generateUrl('admin_category'));
	}

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
