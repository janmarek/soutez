<?php

namespace Jm\EshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Jm\EshopBundle\Entity\User;
use Jm\EshopBundle\Form\UserType;

/**
 * @Route("/admin/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="admin_user")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JmEshopBundle:User')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="admin_user_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $entity = new User();
        $form   = $this->createForm(new UserType(), $entity);

		if ($request->getMethod() === 'POST') {
			$form->bind($request);

			if ($form->isValid()) {
				$plainPass = $form->get('plainPassword')->getData();
				$this->setPassword($entity, $plainPass);

				$em = $this->getDoctrine()->getManager();
				$em->persist($entity);
				$em->flush();

				return $this->redirect($this->generateUrl('admin_user_edit', array('id' => $entity->getId())));
			}
		}

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="admin_user_edit")
     * @Template()
     */
    public function editAction(Request $request, User $entity)
    {
        $editForm = $this->createForm(new UserType(false), $entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

		if ($request->getMethod() === 'POST') {
			$editForm->bind($request);

			if ($editForm->isValid()) {
				if ($plainPass = $editForm->get('plainPassword')->getData()) {
					$this->setPassword($entity, $plainPass);
				}

				$em = $this->getDoctrine()->getManager();
				$em->persist($entity);
				$em->flush();

				return $this->redirect($this->generateUrl('admin_user_edit', array('id' => $entity->getId())));
			}
		}

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }


	/**
	 * @Route("/{id}/delete", name="admin_user_delete")
	 * @Method("POST")
	 */
	public function deleteAction(Request $request, User $entity)
	{
		$em = $this->getDoctrine()->getManager();
		$em->remove($entity);
		$em->flush();

		return $this->redirect($this->generateUrl('admin_user'));
	}

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

	private function setPassword(User $entity, $plainPassword)
	{
		$factory = $this->get('security.encoder_factory');
		$encoder = $factory->getEncoder($entity);
		$password = $encoder->encodePassword($plainPassword, $entity->getSalt());
		$entity->setPassword($password);
	}
}
