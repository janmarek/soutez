<?php

namespace Jm\EshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Jm\EshopBundle\Entity\Product;
use Jm\EshopBundle\Form\AddToBasketType;

/**
 * @Route("/basket")
 */
class BasketController extends Controller
{
	const SESSION_KEY = 'basket';

    /**
	 * @Method("POST")
	 * @Route("/add/{id}", name="basket_add")
     */
    public function addAction(Request $request, Product $product)
    {
		$form = $this->createForm(new AddToBasketType(), array('id' => null, 'amount' => 1));

		$form->bind($request);

		$amount = $form->get('amount')->getData();

		if ($product->isAmountAvailable($amount)) {
			$product->decreaseAmount($amount);

			$em = $this->getDoctrine()->getManager();
			$em->flush();

			$data = $this->getSessionData();
			$data[$product->getId()] = array(
				'id' => $product->getId(),
				'amount' => (int) $amount,
			);
			$request->getSession()->set(self::SESSION_KEY, $data);
		} else {
			$this->get('session')->getFlashBag()
				->add('error', 'Nelze přidat ' . $amount . ' položek, pouze ' . $product->getAmount() . ' je k dispozici.');
		}

		return $this->redirect($this->generateUrl('basket'));
    }

	/**
	 * @Route("", name="basket")
	 * @Template
	 */
	public function viewAction(Request $request)
	{
		$data = $this->getSessionData();
		$basketData = $this->getDoctrine()->getManager()
			->getRepository('JmEshopBundle:Product')
			->getBasketItems($data);

		return array(
			'basketData' => $basketData,
		);
	}

	/**
	 * @Route("/delete/{id}", name="basket_delete")
	 */
	public function deleteAction(Request $request, Product $product)
	{
		$data = $this->getSessionData();
		$amount = $data[$product->getId()]['amount'];
		unset($data[$product->getId()]);
		$request->getSession()->set(self::SESSION_KEY, $data);

		$product->increaseAmount($amount);

		$this->getDoctrine()->getManager()->flush();

		$this->get('session')->getFlashBag()
			->add('notice', 'Položka ' . $product->getAbout() . ' byla odebrána.');

		return $this->redirect($this->generateUrl('basket'));
	}

	private function getSessionData()
	{
		return $this->getRequest()->getSession()->get(self::SESSION_KEY) ?: array();
	}
}
