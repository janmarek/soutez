<?php

namespace Jm\EshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Jm\EshopBundle\Entity\Product;
use Jm\EshopBundle\Form\ProductType;

/**
 * @Route("/admin/product")
 */
class ProductController extends Controller
{
    /**
     * @Route("/", name="admin_product")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JmEshopBundle:Product')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/new", name="admin_product_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $entity = new Product();
        $form   = $this->createForm(new ProductType(), $entity);

		if ($request->getMethod() === 'POST') {
			$form->bind($request);

			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($entity);
				$em->flush();

				return $this->redirect($this->generateUrl('admin_product_edit', array('id' => $entity->getId())));
			}
		}

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     * @Route("/{id}/edit", name="admin_product_edit")
     * @Template()
     */
    public function editAction(Request $request, Product $entity)
    {
        $editForm = $this->createForm(new ProductType(), $entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

		if ($request->getMethod() === 'POST') {
			$editForm->bind($request);

			if ($editForm->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($entity);
				$em->flush();

				return $this->redirect($this->generateUrl('admin_product_edit', array('id' => $entity->getId())));
			}
		}

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * @Route("/{id}/delete", name="admin_product_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request, Product $entity)
    {
		$em = $this->getDoctrine()->getManager();
		$em->remove($entity);
		$em->flush();

        return $this->redirect($this->generateUrl('admin_product'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
