<?php

namespace Jm\EshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Jm\EshopBundle\Entity\Category;
use Jm\EshopBundle\Entity\Product;
use Jm\EshopBundle\Form\AddToBasketType;

/**
 * @Route("/category")
 */
class CategoryFrontController extends Controller
{

	/**
	 * @return \Jm\EshopBundle\Entity\CategoryRepository
	 */
	private function getRepository()
	{
		return $this->getDoctrine()->getManager()->getRepository('JmEshopBundle:Category');
	}

    /**
     * @Template()
     */
    public function menuAction(Category $category = null)
    {
		$categories = $this->getRepository()
			->createQueryBuilder('c')->andWhere('c.parent is null')
			->getQuery()->getResult();

        return array(
            'categories' => $categories,
			'current' => $category,
        );
    }

	/**
	 * @Route("/{id}/detail", name="category_detail")
	 * @Template
	 */
	public function detailAction(Category $category)
	{
		$products = $this->getDoctrine()->getManager()
			->getRepository('JmEshopBundle:Product')
			->findBy(array(
				'category' => $category->getId(),
			));

		$items = array();

		foreach ($products as $product) {
			$items[] = array(
				'product' => $product,
				'form' => $this->createBasketForm($product)->createView(),
			);
		}

		return array(
			'category' => $category,
			'items' => $items,
		);
	}

	private function createBasketForm(Product $product)
	{
		return $this->createForm(new AddToBasketType(), array(
			'id' => $product->getId(),
			'amount' => 1
		));
	}
}
